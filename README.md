# Signing Examples

This repository contains public examples that can be used with the Lightning Signatures app.

When making a scratch org, don't forget to install the app before pushing the source.

## Examples for Account
### accountsample1
[![deploy to Salesforce](https://willikins.appfactorygroup.com/signing-examples/static/deploy-btn.svg.png)](https://willikins.appfactorygroup.com/signing-examples/?file=accountsample1)

## Examples for Contract
### contractsample1
[![deploy to Salesforce](https://willikins.appfactorygroup.com/signing-examples/static/deploy-btn.svg.png)](https://willikins.appfactorygroup.com/signing-examples/?file=contractsample1)

## Examples for Opportunity
### opportunitysample1
[![deploy to Salesforce](https://willikins.appfactorygroup.com/signing-examples/static/deploy-btn.svg.png)](https://willikins.appfactorygroup.com/signing-examples/?file=opportunitysample1)

## Examples for Quote
### quotesample1
[![deploy to Salesforce](https://willikins.appfactorygroup.com/signing-examples/static/deploy-btn.svg.png)](https://willikins.appfactorygroup.com/signing-examples/?file=quotesample1)

## Examples for Order
### ordersample1
[![deploy to Salesforce](https://willikins.appfactorygroup.com/signing-examples/static/deploy-btn.svg.png)](https://willikins.appfactorygroup.com/signing-examples/?file=ordersample1)

## Generic example
### signingsample

[![deploy to Salesforce](https://willikins.appfactorygroup.com/signing-examples/static/deploy-btn.svg.png)](https://willikins.appfactorygroup.com/signing-examples/?file=signingsample)
